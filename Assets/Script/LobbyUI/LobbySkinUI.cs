﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbySkinUI : LobbyLeftTabUI
{
	
	public List<LobbyLinkButton> SexButtons;
	public UISprite m_LobbyCharacter;

	public UISprite m_Normal;
	public UISprite m_Custom;

    public GameObject m_NormalStiky;
    public GameObject m_CustomStiky;

    private const string SELECT_SEX = "SELECT_SEX";
	private const string SELECT_DRESS = "SELECT_DRESS";
	private const string HAS_DRESS = "HAS_DRESS_{0}";
	private const string SPRITENAME_SPLIT = "{0}-{1}";
    private const string BUY_DRESS = "BUY-{0}-{1}";

	private int m_Sex = 0;
	private int m_Kind = 0;

	private void Awake()
	{
		m_Sex = 0;
		if (Util.HasData(SELECT_SEX))
		{
			m_Sex = Util.GetIntData(SELECT_SEX);
		}
		else
		{
			Util.SaveData(SELECT_SEX, m_Sex);
		}

		if (Util.HasData(SELECT_DRESS))
		{
			m_Kind = Util.GetIntData(SELECT_DRESS);
			SelectCharacter(m_Kind);
		}
		else
		{
			Util.SaveData(SELECT_DRESS, m_Kind);
		}

		SetSkinUI();
	}
	
	
	public override void SelectButton(LobbyLinkButton button)
	{
		int sex = SexButtons.IndexOf(button);

		if (sex == m_Sex)
			return;
		
		m_Sex = sex;
		SetSkinUI();
	}

	public void OnClick_Normal()
	{
		SelectCharacter(1);

        string Key = string.Format(BUY_DRESS, m_Sex, 1);
        if (!Util.HasData(Key))
        {
            Util.SaveData(Key, "OK");
            LobbyMainUI LobbyMain = gameObject.GetComponentInParent<LobbyMainUI>();

            LobbyMain.UseGold(5500);
            m_NormalStiky.SetActive(true);
            
        }
	}
	
	public void OnClick_Custom()
	{
		SelectCharacter(2);

        string Key = string.Format(BUY_DRESS, m_Sex, 2);
        if (!Util.HasData(Key))
        {
            Util.SaveData(Key, "OK");

            LobbyMainUI LobbyMain = gameObject.GetComponentInParent<LobbyMainUI>();

            LobbyMain.UseGold(19800);
            m_CustomStiky.SetActive(true);
        }
    }

	private void SetSkinUI()
	{
		for (int Index = 0; Index < SexButtons.Count; ++Index)
		{
			SexButtons[Index].SetSprite(Index.Equals(m_Sex));
		}

		if (m_Kind == 0)
		{
			m_LobbyCharacter.spriteName = string.Format(SPRITENAME_SPLIT, m_Sex, m_Kind);
		}
		SetCharacter();

        m_NormalStiky.SetActive(Util.HasData(string.Format(BUY_DRESS, m_Sex, 1)));
        m_CustomStiky.SetActive(Util.HasData(string.Format(BUY_DRESS, m_Sex, 2)));
    }

	private void SelectCharacter(int kind)
	{
		m_Kind = kind;
		Util.SaveData(SELECT_SEX, m_Sex);
		Util.SaveData(SELECT_DRESS, m_Kind);
		m_LobbyCharacter.spriteName = string.Format(SPRITENAME_SPLIT, m_Sex, m_Kind);
	}

	private void SetCharacter()
	{
		m_Normal.spriteName = string.Format(SPRITENAME_SPLIT, m_Sex, 1);
		m_Custom.spriteName = string.Format(SPRITENAME_SPLIT, m_Sex, 2);
	}

}
