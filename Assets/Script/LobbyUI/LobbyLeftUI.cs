﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyLeftUI : MonoBehaviour
{
	public GameObject[] TabObjects;
	public string[] BGSpriteNames;
	public UISprite BackGroud;
	
	// Use this for initialization
	void Start ()
	{
		SelectTab(0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnClick_LinkTab()
	{
		SelectTab(0);
	}

	public void OnClick_SkinTab()
	{
		SelectTab(1);
	}

	private void SelectTab(int Select)
	{
		for (int Index = 0; Index < TabObjects.Length; ++Index)
		{
			TabObjects[Index].SetActive(Index.Equals(Select));
		}

		BackGroud.spriteName = BGSpriteNames[Select];
	}
}
