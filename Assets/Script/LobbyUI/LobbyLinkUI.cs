﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyLinkUI : LobbyLeftTabUI
{

	public List<LobbyLinkButton> LinkButtons;
	public UIAtlas[] LinkAtlases;
	public string[] LinkMarkNames;
	public string[] LinkAniFilter;

	public UISprite m_LinkMark;
	public UISprite m_LinkSprite;
	public UISpriteAnimation m_LinkAni;

	private const string SELECT_LINK = "SELECT_LINK";
	
	// Use this for initialization
	void Start ()
	{
		int select = 0;
		if (Util.HasData(SELECT_LINK))
		{
			select = Util.GetIntData(SELECT_LINK);
		}
		SelectButton(LinkButtons[select]);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public override void SelectButton(LobbyLinkButton button)
	{
		int Select = LinkButtons.IndexOf(button);
		for (int Index = 0; Index < LinkButtons.Count; ++Index)
		{
			LinkButtons[Index].SetSprite(Select.Equals(Index));
		}

        switch (button.name)
        {
            case "Unicon":
                m_LinkSprite.transform.localPosition = Vector3.down * 20f;
                break;
            default:
                m_LinkSprite.transform.localPosition = Vector3.down * 62f;
                break;
        }

		m_LinkSprite.atlas = LinkAtlases[Select];
		m_LinkAni.namePrefix = LinkAniFilter[Select];
		m_LinkAni.ResetToBeginning();
		m_LinkAni.Play();

		m_LinkMark.spriteName = LinkMarkNames[Select];
		
		Util.SaveData(SELECT_LINK, Select);
	}
}
