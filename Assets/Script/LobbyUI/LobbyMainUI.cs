﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyMainUI : MonoBehaviour
{
	public GameObject[] LobbyWindows;
    public UILabel m_GoldLabel;
    public GameObject m_Popup;

    private const string GOLD_KEY = "GOLD_COUNT";
    private SoundManager m_SoundManager;

    private int m_GoldCount;

    public void ShowPopup(int Episode)
    {
        m_Popup.SetActive(true);

        UISprite sprite = m_Popup.GetComponentInChildren<UISprite>();

        switch (Episode)
        {
            case 201:
                sprite.spriteName = "에피소드2 활성화 안내";
                break;
            case 301:
                sprite.spriteName = "에피소드3 활성화 안내";
                break;
        }
    }

    public void ClosePopup()
    {
        m_Popup.SetActive(false);
    }

    // Use this for initialization
    void Start ()
	{
		OpenWindow(0);

        m_SoundManager = GameObject.FindObjectOfType<SoundManager>();

        if (m_SoundManager == null)
        {
            GameObject soundManager = new GameObject("SoundManager");
            m_SoundManager = soundManager.AddComponent<SoundManager>();
        }
        m_SoundManager.SoundPlay(BGMFlag.Main_BGM);


        if (!Util.HasData(GOLD_KEY))
        {
            m_GoldCount = 99999;
            Util.SaveData(GOLD_KEY, m_GoldCount);
        }
        else
            m_GoldCount = Util.GetIntData(GOLD_KEY);

        SetGoldLabel();
    }

    private void SetGoldLabel()
    {
        m_GoldLabel.text = m_GoldCount.ToString("#,###");
    }

    public void UseGold(int gold)
    {
        m_GoldCount -= gold;
        Util.SaveData(GOLD_KEY, m_GoldCount);
        SetGoldLabel();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OpenWindow(int Select)
	{
		for (int Index = 0; Index < LobbyWindows.Length; ++Index)
		{
			LobbyWindows[Index].SetActive(Index.Equals(Select));
		}
	}

	[ContextMenu("DeleteData")]
	private void DeleteData()
	{
		PlayerPrefs.DeleteAll();
	}
	
	[ContextMenu("SaveStage")]
	private void SaveStage()
	{
		Util.SaveData("EPISODE_STAGEKEY", 201);
	}

	public void OpenNaverCafe()
	{
		Application.OpenURL("https://cafe.naver.com/palpitofastrun.cafe");
	}

	public void GotoMainLobby()
	{
		OpenWindow(0);
	}

	public void GotoWorldMap()
	{
		OpenWindow(1);
	}
	
	public void GotoEpisode1()
	{
		OpenWindow(2);
	}
	
	public void GotoEpisode2()
	{
		OpenWindow(3);
	}
	
	public void GotoEpisode3()
	{
		OpenWindow(4);
	}
}
