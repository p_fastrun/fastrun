﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldMapStageLink : MonoBehaviour
{
	public UISprite m_Sprite;
	public UISprite m_Clear;

	public int m_StageId;


	private const string EPISODE_STAGEKEY = "EPISODE_STAGEKEY";
	private const string PLAY_EPISODE_STAGEKEY = "PLAY_EPISODE_STAGE";
	private bool IsLock = false;
	
	// Use this for initialization
	void Start ()
	{
		int StageID = 0;

		if (!Util.HasData(EPISODE_STAGEKEY))
		{
			StageID = 101;
			Util.SaveData(EPISODE_STAGEKEY, StageID);
		}
		else
		{
			StageID = Util.GetIntData(EPISODE_STAGEKEY);	
		}

		if (m_StageId < StageID)
		{
			m_Clear.gameObject.SetActive(true);
		}
		else if (m_StageId.Equals(StageID))
		{
			m_Clear.gameObject.SetActive(false);
		}
		else if (m_StageId > StageID)
		{
			IsLock = true;
			m_Clear.gameObject.SetActive(false);
			m_Sprite.spriteName = m_Sprite.spriteName + "_잠금";
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnClick()
	{
		if (IsLock)
		{
			return;
		}
		
		Util.SaveData(PLAY_EPISODE_STAGEKEY, m_StageId);
		Util.LoadLevel("MainGameScene");
	}
}
