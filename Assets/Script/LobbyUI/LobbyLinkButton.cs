﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyLinkButton : MonoBehaviour
{
	public LobbyLeftTabUI m_LinkUI;
	public UISprite m_Sprite;
	public string m_SpriteName;

    public int First;
    public int Second;
    public int Limite;

    private int StageID;

	public void Awake()
	{
		m_SpriteName = m_Sprite.spriteName;
		m_SpriteName = m_SpriteName.Replace("2", "{0}");

        StageID = Util.GetIntData("EPISODE_STAGEKEY");

        if (StageID < Limite)
        {
            m_Sprite.spriteName = string.Format(m_SpriteName, 3);
        }
	}

	public void OnClick()
	{
        if (StageID < Limite)
        {
            LobbyMainUI LobbyMain = gameObject.GetComponentInParent<LobbyMainUI>();
            LobbyMain.ShowPopup(Limite);
            return;
        }

        m_LinkUI.SelectButton(this);
	}

	public void SetSprite(bool isSelect)
	{
        if (StageID < Limite)
        {
            m_Sprite.spriteName = string.Format(m_SpriteName, 3);
        }
        else
            m_Sprite.spriteName = string.Format(m_SpriteName, isSelect ? First : Second);
	}
}
