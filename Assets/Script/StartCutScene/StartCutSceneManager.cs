﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCutSceneManager : MonoBehaviour
{

	public UISpriteAnimation m_Ani;

	public UISprite m_MainLogo;
	public GameObject m_IPPopup;
	
	private const string CutSceneKey = "CunScene";
	private const float StartDelay = 3f;

	private float CheckTime = 0;
	
	void Awake()
	{
		m_Ani.Pause();
		if (Util.HasData(CutSceneKey))
		{
			NextScene();
		}
	}

	private void Update()
	{
		if (CheckTime < StartDelay)
		{
			CheckTime += Time.deltaTime;
			
			if (CheckTime >= StartDelay &&
			    !m_Ani.isPlaying)
			{
				m_Ani.Play();
			}
			return;
		}
		
		if (!m_Ani.isPlaying)
		{
			NextScene();
		}
	}

	public void OnClick_CutScene()
	{
		if(!Util.HasData(CutSceneKey))
			NextScene();
	}

	public void OnClick_SoundIP()
	{
		m_IPPopup.SetActive(!m_IPPopup.activeInHierarchy);
	}

	private void NextScene()
	{
		Util.SaveData(CutSceneKey, "OK");
		m_Ani.gameObject.SetActive(false);
		m_MainLogo.gameObject.SetActive(true);
	}

	public void GoToLobbyScene()
	{
		Util.LoadLevel(Util.LobbyScene);
	}

	[ContextMenu("DeleteData")]
	private void DeleteData()
	{
		PlayerPrefs.DeleteAll();
	}
}
