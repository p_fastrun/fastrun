﻿using UnityEngine;
using UnityEngine.SceneManagement;

public static class Util
{
	public const string LobbyScene = "LobbyScene";
	public const int ScreenX = 854;
	public const int ScreenY = 480;

	public static void LoadLevel(string name)
	{
		SceneManager.LoadScene(name);
	}

	public static bool HasData(string Key)
	{
		return PlayerPrefs.HasKey(Key);
	}

	public static void SaveData(string Key, string data)
	{
		PlayerPrefs.SetString(Key, data);
	}
	
	public static void SaveData(string Key, int data)
	{
		PlayerPrefs.SetInt(Key, data);
	}

	public static string GetStringData(string Key)
	{
		return PlayerPrefs.GetString(Key);
	}

	public static int GetIntData(string Key)
	{
		return PlayerPrefs.GetInt(Key);
	}
}
