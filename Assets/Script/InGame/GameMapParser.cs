﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMapParser
{
	private List<InGameObjectData> m_DataList = new List<InGameObjectData>();
	
	public void LoadMap(int StageID)
	{
		int Episode = StageID / 100;
		int Stage = StageID % 10;


		string StageName = string.Format("EP0{0}_0{1}", Episode, Stage);
		string LoadPath = "GameData/" + StageName;
		
		TextAsset text = UnityEngine.Resources.Load<TextAsset>(LoadPath);
		
		System.IO.MemoryStream ms = new System.IO.MemoryStream(text.bytes);
		System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bin = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
		bin.Binder = new BinalryBinder();
		InGameObjectData[] ConvertData = (InGameObjectData[])bin.Deserialize(ms);

		if (ConvertData != null)
		{
			m_DataList.Clear();
			m_DataList.AddRange(ConvertData);
			m_DataList.Sort((A, B) => { return A.PosX.CompareTo(B.PosX); });
		}
	}

	public List<InGameObjectData> GetRangeObject(float min, float max)
	{
		List<InGameObjectData> RangeList = new List<InGameObjectData>();
		for (int Index = 0; Index < m_DataList.Count; ++Index)
		{
			float PosX = -m_DataList[Index].PosX;
			
			if (min <= PosX && max >= PosX)
			{
				RangeList.Add(m_DataList[Index]);
			}
			else
			{
                continue;
			}
		}

		for (int Index = 0; Index < RangeList.Count; ++Index)
		{
			m_DataList.Remove(RangeList[Index]);
		}

		return RangeList;
	}

	public List<CharacterColorInfo> GetColorInfos(UIAtlas[] AtlasList)
	{
		List<InGameObjectData> filterList = new List<InGameObjectData>();

		for (int Index = 0; Index < m_DataList.Count; ++Index)
		{
			InGameObjectData CheckData = filterList.Find(d => d.AtlasIdx.Equals(m_DataList[Index].AtlasIdx) &&
			                                                  d.spriteName.Equals(m_DataList[Index].spriteName));

			if (CheckData != null)
				continue;

			if (!m_DataList[Index].ObjectType.Equals((int)InGameObjectType.Trap) &&
			    !m_DataList[Index].ObjectType.Equals((int)InGameObjectType.AniTrap))
				continue;

			filterList.Add(m_DataList[Index]);
		}

		List<CharacterColorInfo> ColorInfoList = new List<CharacterColorInfo>();

		for (int Index = 0; Index < filterList.Count; ++Index)
		{
			UIAtlas atlas = AtlasList[filterList[Index].AtlasIdx];
			
			if (filterList[Index].ObjectType.Equals((int) InGameObjectType.Trap))
			{
				ColorInfoList.Add(new CharacterColorInfo(atlas, filterList[Index].spriteName));
			}
			else if (filterList[Index].ObjectType.Equals((int) InGameObjectType.AniTrap))
			{
				string Temp = filterList[Index].spriteName.Replace("-1", string.Empty);

                if (atlas.spriteList.Find(d => d.name.Contains(Temp)) == null)
                {
                    ColorInfoList.Add(new CharacterColorInfo(atlas, filterList[Index].spriteName));
                }
                else
                {
                    for (int Index2 = 0; Index2 < atlas.spriteList.Count; ++Index2)
                    {
                        if (atlas.spriteList[Index2].name.Contains(Temp))
                        {
                            ColorInfoList.Add(new CharacterColorInfo(atlas, atlas.spriteList[Index2].name));
                        }
                    }
                }
				
			}
		}

		return ColorInfoList;
	}


	public class BinalryBinder : System.Runtime.Serialization.SerializationBinder
	{

		public override System.Type BindToType(string assemblyName, string typeName)
		{
			string assem = System.Reflection.Assembly.GetExecutingAssembly().FullName;

			System.Type type = System.Type.GetType(string.Format("{0}, {1}", typeName, assem));

			return type;
		}
	}
	
	
}
