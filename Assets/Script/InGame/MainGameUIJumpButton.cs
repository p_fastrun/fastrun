﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameUIJumpButton : MonoBehaviour {

    public MainGameManager m_GameManager;
    public UISprite m_Sprite;

    private const string JumpActive = "점프(활성화)";
    private const string JumpDisable = "점프(비활성화)";
    private bool m_IsPress;


    public void OnPress(bool isPress)
    {
        if (m_IsPress.Equals(isPress))
        {
            return;
        }

        m_IsPress = isPress;

        m_Sprite.spriteName = m_IsPress ? JumpActive : JumpDisable;

        if (!m_IsPress)
            return;

        m_GameManager.OnClick_Jump();
    }
	
}
