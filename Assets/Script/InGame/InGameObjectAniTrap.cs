﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameObjectAniTrap : InGameObjectBase
{

	public UISpriteAnimation m_SpriteAni;
	
	public override void SetData(UIAtlas atlas , InGameObjectData data)
	{
		base.SetData(atlas, data);

		string anispritename = data.spriteName;

        anispritename = anispritename.Substring(0, anispritename.Length - 2);

		m_SpriteAni.namePrefix = anispritename;
		m_SpriteAni.RebuildSpriteList();
		m_SpriteAni.frameIndex = 0;
		m_SpriteAni.Play();
	}
}
