﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameLinkEgle : MainGameLinkBase {

	protected override void SetAtlas()
	{
		m_AniStartName += "흰머리수리-";
		m_Atlas = Resources.Load<UIAtlas>(LINKATLAS_PATH + "Link_Egle");
	}
	
	public override void SetSelectBehavior()
	{
		m_GameCharacter.transform.localPosition = new Vector3(-150f, 130f);
	}

	public override void SetSelectEnd()
	{
		m_GameCharacter.transform.localPosition = new Vector3(-150f, -130f);
	}
	
	protected override void SetAni(UISpriteAnimation SpriteAni, CharacterState state)
	{
		int Frames = 0;
		string AniName = string.Empty;
		
		switch (state)
		{
			case CharacterState.Run:
				AniName = "날기";
				Frames = 5;
				break;
			
			case CharacterState.Collision:
				AniName = "아픔";
				Frames = 5;
				SpriteAni.loop = false;
				SpriteAni.m_EndCallBack = m_GameCharacter.CollisionAniEnd;
				break;
			case CharacterState.Jump:
			case CharacterState.DoubleJump:
			case CharacterState.Landing:
                break;
			case CharacterState.Defeated:
			case CharacterState.Virtory:
                m_GameCharacter.ChangeBehaviour();
                return;
		}

		SpriteAni.framesPerSecond = Frames;
		SpriteAni.namePrefix = m_AniStartName + AniName;
		SpriteAni.RebuildSpriteList();
		SpriteAni.ResetToBeginning();
		m_GameCharacter.m_Sprite.ResizeCollider();
	}
}
