﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameLinkUnicorn : MainGameLinkBase
{
	protected override void SetAtlas()
	{
		m_AniStartName += "유니콘-";
		m_Atlas = Resources.Load<UIAtlas>(LINKATLAS_PATH + "Link_Unicon");
	}
	
	public override bool IsImpossibleProcessing(CharacterState state)
	{
		switch (state)
		{
			case CharacterState.Run:
			case CharacterState.Collision:
			case CharacterState.Jump:
			case CharacterState.Landing:
				return false;
			case CharacterState.DoubleJump:
			case CharacterState.Defeated:
			case CharacterState.Virtory:
				break;
		}
		return true;
	}

    public override void SetSelectBehavior()
	{
        m_GameCharacter.transform.localPosition = new Vector3(-150f, -100f);
        m_GameManager.ScrollSpeedUp();
		
	}

	public override void SetSelectEnd()
	{
        m_GameCharacter.transform.localPosition = new Vector3(-150f, -130f);
        m_GameManager.ScrollSpeedNormal();
	}
	
	protected override void SetAni(UISpriteAnimation SpriteAni, CharacterState state)
	{
		int Frames = 0;
		string AniName = string.Empty;
		
		switch (state)
		{
			case CharacterState.Run:
				AniName = "달리기";
				Frames = 5;
				break;
			
			case CharacterState.Collision:
				AniName = "아픔";
				Frames = 5;
				SpriteAni.loop = false;
				SpriteAni.m_EndCallBack = m_GameCharacter.CollisionAniEnd;
				break;
			case CharacterState.Jump:
			case CharacterState.Landing:
            case CharacterState.DoubleJump:
                break;				
			case CharacterState.Defeated:
			case CharacterState.Virtory:
                m_GameCharacter.ChangeBehaviour();
                return;
		}

		SpriteAni.framesPerSecond = Frames;
		SpriteAni.namePrefix = m_AniStartName + AniName;
		SpriteAni.RebuildSpriteList();
		SpriteAni.ResetToBeginning();
		m_GameCharacter.m_Sprite.ResizeCollider();
	}
	
	protected override void CollisionTrap(MainGameManager GameManager, InGameObjectBase ObjBase)
	{
		if (ObjBase.transform.localPosition.y < 0)
		{
			ObjBase.ReturnObjectPool();
		}
		else
		{
			m_GameCharacter.SetAniState(CharacterState.Collision);
			GameManager.ScrollStop();
		}
	}
}
