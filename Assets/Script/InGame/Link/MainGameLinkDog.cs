﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameLinkDog : MainGameLinkBase
{
	protected override void SetAtlas()
	{
		m_AniStartName += "웰시코기-";
		m_Atlas = Resources.Load<UIAtlas>(LINKATLAS_PATH + "Link_Dog");
	}
	
	public override void SetSelectBehavior()
	{
		m_GameManager.ScrollSpeedUp();
	}

	public override void SetSelectEnd()
	{
		m_GameManager.ScrollSpeedNormal();
	}
	
	protected override void SetAni(UISpriteAnimation SpriteAni, CharacterState state)
	{
		int Frames = 0;
		string AniName = string.Empty;
		
		switch (state)
		{
			case CharacterState.Run:
				AniName = "달리기";
				Frames = 10;
				break;
			
			case CharacterState.Collision:
				AniName = "아픔";
				Frames = 5;
				SpriteAni.loop = false;
				SpriteAni.m_EndCallBack = m_GameCharacter.CollisionAniEnd;
				break;
			case CharacterState.Jump:
			case CharacterState.DoubleJump:
			case CharacterState.Landing:
			case CharacterState.Defeated:
			case CharacterState.Virtory:
				m_GameCharacter.ChangeBehaviour();
				return;
		}

		SpriteAni.framesPerSecond = Frames;
		SpriteAni.namePrefix = m_AniStartName + AniName;
		SpriteAni.RebuildSpriteList();
		SpriteAni.ResetToBeginning();
		m_GameCharacter.m_Sprite.ResizeCollider();
	}
}

