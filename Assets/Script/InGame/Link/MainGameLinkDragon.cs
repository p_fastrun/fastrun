﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameLinkDragon : MainGameLinkBase
{
	private float m_Percent;
	private UIAtlas m_SubAtlas;
    private UIAtlas m_MainAtlas;

    private string m_MainAniName;
    private string m_SubAniName;

    private bool isDragonBaby = false;
    private CharacterState m_State;


    protected override void SetAtlas()
	{		
        m_MainAniName = "드래곤-";
        m_SubAniName = "드래곤새끼-";

        m_MainAtlas = Resources.Load<UIAtlas>(LINKATLAS_PATH + "Link_Dragon");
        m_SubAtlas = Resources.Load<UIAtlas>(LINKATLAS_PATH + "Link_DragonBaby");

        m_Atlas = m_MainAtlas;
        m_AniStartName = m_MainAniName;
    }
	
	public override void SetSelectBehavior()
	{
		m_GameCharacter.transform.localPosition = new Vector3(-150f, 40);
	}

    public override void SetSelectEnd()
    {
        isDragonBaby = false;
        m_Atlas = m_MainAtlas;
        m_AniStartName = m_MainAniName;
	    m_GameCharacter.transform.localPosition = new Vector3(-150f, -130);
    }

    public override bool IsImpossibleProcessing(CharacterState state)
	{
		switch (state)
		{
			case CharacterState.Run:
			case CharacterState.Collision:
				return false;
			case CharacterState.Jump:
			case CharacterState.DoubleJump:
			case CharacterState.Landing:
			case CharacterState.Defeated:
			case CharacterState.Virtory:
				break;
		}
		return true;
	}
	
	public override void SetLinkPercent(float Percent)
	{
        if (Percent <= 0.5f && m_Percent > 0.5f)
        {
            isDragonBaby = true;
            m_Atlas = m_SubAtlas;
            m_AniStartName = m_SubAniName;
            SetAniMation(m_GameCharacter.m_SpriteAni, m_State);
	        m_GameCharacter.transform.localPosition = new Vector3(-150f, -130);
        }

        m_Percent = Percent;
    }
	
	protected override void SetAni(UISpriteAnimation SpriteAni, CharacterState state)
	{
		int Frames = 0;
		string AniName = string.Empty;
        m_State = state;

        if (isDragonBaby == false)
        {
            switch (m_State)
            {
                case CharacterState.Run:
                    AniName = "날기";
                    Frames = 5;
                    break;

                case CharacterState.Collision:
                case CharacterState.Jump:
                case CharacterState.DoubleJump:
                case CharacterState.Landing:
                    break;
                case CharacterState.Defeated:
                case CharacterState.Virtory:
                    m_GameCharacter.ChangeBehaviour();
                    return;
            }
        }
        else
        {
            switch (m_State)
            {
                case CharacterState.Run:
                    AniName = "달리기";
                    Frames = 5;
                    break;
                case CharacterState.Collision:
                    AniName = "아픔";
                    Frames = 5;
                    SpriteAni.loop = false;
                    SpriteAni.m_EndCallBack = m_GameCharacter.CollisionAniEnd;
                    break;
                case CharacterState.Jump:
                case CharacterState.DoubleJump:
                case CharacterState.Landing:
                    break;
                
                case CharacterState.Defeated:
                case CharacterState.Virtory:
                    m_GameCharacter.ChangeBehaviour();
                    return;
            }
        }
		
		

		SpriteAni.framesPerSecond = Frames;
		SpriteAni.namePrefix = m_AniStartName + AniName;
		SpriteAni.RebuildSpriteList();
		SpriteAni.ResetToBeginning();
		m_GameCharacter.m_Sprite.ResizeCollider();
	}
	
	protected override void CollisionTrap(MainGameManager GameManager, InGameObjectBase ObjBase)
	{
        if (isDragonBaby == false)
            ObjBase.ReturnObjectPool();
        else if (isDragonBaby == true && ObjBase.transform.localPosition.y < 0)
            ObjBase.ReturnObjectPool();
        else
        {
            m_GameCharacter.SetAniState(CharacterState.Collision);
            GameManager.ScrollStop();
        }
	}
	
	
}
