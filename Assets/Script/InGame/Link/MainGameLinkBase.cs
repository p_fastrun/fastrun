﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameLinkBase : CharacterSpriteControlor
{
    public override bool IsChoice(bool IsLinkPress)
    {
        return IsLinkPress;
    }

    public override bool IsImpossibleJump()
    {
        return true;
    }

    public override bool IsImpossibleProcessing(CharacterState state)
    {
        switch (state)
        {
            case CharacterState.Run:
            case CharacterState.Collision:
                return false;
            case CharacterState.Jump:
            case CharacterState.DoubleJump:
            case CharacterState.Landing:
            case CharacterState.Defeated:
            case CharacterState.Virtory:
                break;
        }
        return true;
    }
}
