﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameEffectObject : InGameObjectBase
{
    public UISpriteAnimation m_SpriteAni;

    public override void Reset()
    {
        m_Sprite.depth = 5;
        m_SpriteAni.ResetToBeginning();
        m_SpriteAni.m_EndCallBack = ReturnObjectPool;
    }
}
