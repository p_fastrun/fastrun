﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Xsl;
using UnityEngine;

public class MainGameUILinkButton : MonoBehaviour
{
	public UISprite m_LinkButton;
	public UISprite m_CorverSprite;

	public const int LINK_MAX = 20;
	private const string MAX_COUNT_SPRITENAME = "링크(틀)";
	private const string LACK_CONT_SPRITENAME = "링크(비활성화)";

	private float m_CoinCount;
	private bool m_LinkPress = false;
	private MainGameManager m_GameManager;

	public void Init(MainGameManager gameManager)
	{
		m_GameManager = gameManager;
	}

	public void AddCoin(float Count)
	{
		if(LINK_MAX >= Count)
			m_CoinCount = Count;
		
		SetCorverSprite();

		if (m_LinkPress && m_CoinCount <= 0)
		{
			m_LinkPress = false;
			m_GameManager.OnPress_LinkButton(m_LinkPress);
		}
	}

	public void OnPress(bool isPress)
	{
		if (!m_LinkPress && isPress && m_CoinCount >= LINK_MAX)
		{
			m_LinkPress = true;
		}
		else if(m_LinkPress && (m_CoinCount <= 0 || !isPress) )
		{
			m_LinkPress = false;
			m_GameManager.OnPress_LinkButton(m_LinkPress);
		}

		if(m_LinkPress)
			m_GameManager.OnPress_LinkButton(m_LinkPress);
	}

	private void SetCorverSprite()
	{
		float amount = m_CoinCount / (float) LINK_MAX;
		amount = amount >= 1f ? 1f : amount;
		m_LinkButton.fillAmount = amount;
		
		
		if (m_CoinCount >= LINK_MAX)
		{
			m_CorverSprite.spriteName = MAX_COUNT_SPRITENAME;
		}
		else
		{
			m_CorverSprite.spriteName = LACK_CONT_SPRITENAME;
		}
	}
}
