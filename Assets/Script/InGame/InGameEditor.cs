﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameEditor : MonoBehaviour
{
	public InGameObjectBase m_ObjectPrefab;
	public InGameObjectBase m_CoinPrefab;
	public InGameObjectBase m_GoalPrefab;
	public Transform[] m_ObjectRoots;
	public float[] m_CoinYPositions;

	public UIAtlas m_Atlas;
	public string m_SpriteName;
	public int m_GroudSize;

	private const int ScreenWidth = 854;


	
	public void SetGroundObject()
	{
		if (m_GroudSize <= 0)
		{
			Debug.LogError("GraoundSize를 지정해주세요");
			return;
		}
		
		GameObject ObjectBase = GameObject.Instantiate(m_ObjectPrefab.gameObject);

		if (ObjectBase == null)
		{
			Debug.LogError("오브젝트 파일이 없습니다.");
			return;
		}

		InGameObjectBase IngameObj = ObjectBase.GetComponent<InGameObjectBase>();

		SetInGameObject(IngameObj, InGameObjectType.Ground);
		IngameObj.m_Sprite.type = UIBasicSprite.Type.Sliced;
		
		int width = IngameObj.m_Sprite.width;
		int MaxCount = m_GroudSize/ width;

		if (width % m_GroudSize > 0)
		{
			MaxCount++;
		}

		float StartX = -ScreenWidth / 2 + (width / 2);
		float StartY = -200f;

		for (int Index = 0; Index < MaxCount; ++Index)
		{
			float PosX = StartX + (Index * width);
			
			GameObject GroundObj = GameObject.Instantiate(IngameObj.gameObject);
			GroundObj.transform.parent = m_ObjectRoots[(int) InGameObjectType.Ground];
			GroundObj.transform.localScale = Vector3.one;
			GroundObj.transform.localPosition = new Vector3(PosX, StartY);
			GroundObj.name = "Track";
		}
		
		GameObject.DestroyImmediate(ObjectBase);
	}

	private void SetInGameObject(InGameObjectBase IngameObj, InGameObjectType ObjType)
	{
		IngameObj.m_ObjectType = ObjType;
		IngameObj.m_Sprite.atlas = m_Atlas;
		IngameObj.m_Sprite.spriteName = m_SpriteName;
		
		IngameObj.m_Sprite.MakePixelPerfect();
		IngameObj.m_Sprite.ResizeCollider();
	}

	public void CreateTrabObject(Vector3 vPos)
	{
		GameObject ObjectBase = GameObject.Instantiate(m_ObjectPrefab.gameObject);

		if (ObjectBase == null)
		{
			Debug.LogError("오브젝트 파일이 없습니다.");
			return;
		}

		InGameObjectBase IngameObj = ObjectBase.GetComponent<InGameObjectBase>();

        InGameObjectType ObjType = InGameObjectType.Trap;
        if (m_SpriteName.Contains("-1"))
        {
            ObjType = InGameObjectType.AniTrap;
        }

		SetInGameObject(IngameObj, ObjType);
		IngameObj.transform.parent = m_ObjectRoots[(int) InGameObjectType.Trap];
		IngameObj.transform.localScale = Vector3.one;
		IngameObj.transform.localPosition = vPos;
		IngameObj.name = "Trap";
	}

	public void CreateCoinObject(Vector3 vPos)
	{
		GameObject CoinClone = GameObject.Instantiate(m_CoinPrefab.gameObject);
		
		CoinClone.transform.parent = m_ObjectRoots[(int) InGameObjectType.Coin];
		CoinClone.transform.localScale = Vector3.one;
		CoinClone.transform.localPosition = vPos;
		CoinClone.name = "coin";
	}

    public void CreateGoalObject(Vector3 vPos)
    {
	    GameObject Goal = GameObject.Instantiate(m_GoalPrefab.gameObject);

        Goal.transform.parent = m_ObjectRoots[(int)InGameObjectType.Ground];
        Goal.transform.localScale = Vector3.one;
        Goal.transform.localPosition = vPos;
        Goal.name = "Goal";
    }
}
