﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterColorInfo
{
	public string m_SpriteName;
	private Color[] m_Pixcels;
	private int m_Width;
	private int m_Height;
	private Rect m_Rect;

	public CharacterColorInfo(UIAtlas atlas, string SpriteName )
	{
		m_SpriteName = SpriteName;

		UISpriteData USD = atlas.GetSprite(m_SpriteName);

		m_Width = USD.width;
		m_Height = USD.height;

		Texture2D tex = atlas.spriteMaterial.mainTexture as Texture2D;

		m_Pixcels = tex.GetPixels(USD.x, tex.height - USD.y - USD.height, USD.width, USD.height);
	}

	public bool CheckPixcelCollision(CharacterColorInfo Info, Vector3 vfrom, Vector3 vTarget)
	{
		Rect rt1 = GetRect(vfrom);
		Rect rt2 = Info.GetRect(vTarget);

		Rect ColRect = GetCollisionRect(rt1, rt2);
		
		rt1 = Rect.MinMaxRect(ColRect.xMin - rt1.xMin, ColRect.yMin - rt1.yMin, 
			ColRect.xMax - rt1.xMin, ColRect.yMax - rt1.yMin);
		rt2 = Rect.MinMaxRect(ColRect.xMin - rt2.xMin, ColRect.yMin - rt2.yMin, 
			ColRect.xMax - rt2.xMin, ColRect.yMax - rt2.yMin);

		
		Color[] col1 = GetColor(rt1);
		Color[] col2 = Info.GetColor(rt2);

		for (int Index = 0; Index < col1.Length; ++Index)
		{
			if (col1.Length <= Index || col2.Length <= Index)
				continue;

			if (col1[Index].a >= 0.5f && 
			    col2[Index].a >= 0.1f)
				return true;

			if (col2[Index].a >= 0.5f && 
			    col1[Index].a >= 0.1f)
				return true;
		}
		return false;
	}
	
	private Rect GetRect(Vector3 vPos)
	{
		return Rect.MinMaxRect(vPos.x - m_Width * 0.5f, vPos.y - m_Height * 0.5f, 
			vPos.x + m_Width * 0.5f, vPos.y + m_Height * 0.5f);
	}
	
	private Color[] GetColor(Rect rt)
	{
		int xMin = Mathf.Abs((int)rt.xMin);
		int Width = Mathf.Abs((int)(rt.xMax - rt.xMin));
		int yMin = Mathf.Abs((int)rt.yMin);
		int Height = Mathf.Abs((int)(rt.yMax - rt.yMin));

		if (Width <= 0 || Height <= 0)
		{
			
			Width = Width <= 0 ? 1 : Width;
			Height = Height <= 0 ? 1 : Height;
		}

		if (xMin + Width > m_Width)
		{
            
			//1정도의 오차 발생
			if (xMin + Width - m_Width == 1)
			{
				if (Width == 1)
					--xMin;
				else
					Width--;
			}
			
		}
		else if (yMin + Height > m_Height)
		{
			//1정도의 오차 발생
			if (yMin + Height - m_Height == 1)
			{
				if (Height == 1)
					--yMin;
				else
					Height--;
			}
			
		}

		List<Color> colors = new List<Color>();

		for (int Index = 0; Index < Height; ++Index)
		{
			for (int Index2 = 0; Index2 < Width; ++Index2)
			{
				int idx = Index * Width + Index2;
				colors.Add(m_Pixcels[idx]);
				
			}
		}

		return colors.ToArray();
	}
	
	private Rect GetCollisionRect(Rect rt1, Rect rt2)
	{
		float Left = rt1.xMin > rt2.xMin ? rt1.xMin : rt2.xMin;
		float Right = rt1.xMax < rt2.xMax ? rt1.xMax : rt2.xMax;
		float Top = rt1.yMin > rt2.yMin ? rt1.yMin : rt2.yMin;
		float Bottom = rt1.yMax < rt2.yMax ? rt1.yMax : rt2.yMax;

		return Rect.MinMaxRect(Left, Top, Right, Bottom);
	}
}
