﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterState
{
	Start,
	Run,
	Jump,
	DoubleJump,
	Landing,
	Collision,
	Defeated,
	Virtory,
}

public class MainGameCharacter : MonoBehaviour
{
	public UISprite m_Sprite;
	public UISpriteAnimation m_SpriteAni;

	public CharacterState m_PrevState;
	public CharacterState m_State;
    public CharacterState m_NextState = CharacterState.Run;
	public float m_JumpPower;
	public float m_JumpDuration;
	public float m_RotateTime;
	
	private const string SELECT_SEX = "SELECT_SEX";
	private const string SELECT_DRESS = "SELECT_DRESS";
	private const string ATLAS_PATH = "Atlas/Character/";
	private const string LINKATLAS_PATH = "Atlas/Link/";
	private const string SELECT_LINK = "SELECT_LINK";
    private const string Action_JumpEndDown = "JumpEndDown";
    private const string Action_LandingEnd = "LandingEnd";

    private MainGameManager m_GameManager;
	private CharacterSpriteControlor m_HumunBehaviour;
	private CharacterSpriteControlor m_LinkBehaviour;
	private CharacterSpriteControlor m_CurrentBehaviour;
	
	private string m_AniStartName = string.Empty;
	private Vector3 m_GroundPos = new Vector3(-150f, -120f, 0);

	private bool m_IsJumpStart = false;
	private float m_JumpTime = 0;
	private float m_JumpPosY = 0;

	private float m_DogCount = 0;

	public string GetSPriteName()
	{
		return m_Sprite.spriteName;
	}

	public void Init(MainGameManager GameManager)
	{
		m_GameManager = GameManager;
		m_HumunBehaviour = CharacterSpriteControlor.CreateFactory(GameManager, this, 100);
		m_LinkBehaviour = CharacterSpriteControlor.CreateFactory(GameManager, this, Util.GetIntData(SELECT_LINK));

		m_CurrentBehaviour = m_HumunBehaviour;
		SetAniState(CharacterState.Run);
		
	}

	private void FixedUpdate()
	{
		if (m_IsJumpStart == false)
			return;
		
		float height = (m_JumpTime * m_JumpTime * (-9.8f) * 0.5f) + (m_JumpTime * m_JumpPower);

		Vector3 vPos = transform.localPosition;
		vPos.y = m_JumpPosY + height;
		
		m_JumpTime += Time.deltaTime * m_JumpDuration;

		if (m_GroundPos.y > vPos.y)
		{
			vPos.y = m_GroundPos.y;
			m_IsJumpStart = false;
			m_JumpTime = 0;
			SetAniState(CharacterState.Run);
		}

		if (m_State == CharacterState.Jump )
		{
			if (transform.localPosition.y > vPos.y)
			{
				SetAniState(CharacterState.Landing);
			}
		}

		transform.localPosition = vPos; 
	}

	private void Update()
	{
		if (m_CurrentBehaviour is MainGameLinkDog)
		{
			m_DogCount += Time.deltaTime;

			if (m_DogCount >= 0.4f)
			{
				Vector3 vEffect = m_GameManager.m_MapProcess.transform.localPosition;
				vEffect.x = -vEffect.x;
				vEffect += transform.localPosition;
				m_GameManager.m_ObjectPool.ParentInObject(
					m_GameManager.m_MapProcess.transform, vEffect, InGameObjectType.DogEffect);

				m_DogCount = 0;
			}
			
		}
	}


	public List<CharacterColorInfo> GetColorInfos()
	{
		List<CharacterColorInfo> ColorInfos = m_HumunBehaviour.GetColorInfos();
		ColorInfos.AddRange(m_LinkBehaviour.GetColorInfos());

		return ColorInfos;
	}

	public void SetLinkPercent(float percent)
	{
        m_CurrentBehaviour.SetLinkPercent(percent);
	}

	public void OnClick_Jump()
	{
        if (m_CurrentBehaviour.IsImpossibleJump())
            return;

		switch (m_State)
		{
			
			case CharacterState.Run:
				m_JumpPosY = m_GroundPos.y;
				SetAniState(CharacterState.Jump);
				break;
			case CharacterState.Landing:
			case CharacterState.Jump:

				if (m_PrevState == CharacterState.DoubleJump)
					return;

				m_JumpTime = 0;
				m_JumpPosY = transform.localPosition.y;
				SetAniState(CharacterState.DoubleJump);
				transform.localRotation = Quaternion.Euler(0, 0, 359f);
				TweenRotation TR = TweenRotation.Begin(gameObject, m_RotateTime, Quaternion.identity);
			 	TR.quaternionLerp = false;
				TR.callWhenFinished = "JumpEndDown";
				TR.eventReceiver = this.gameObject;
				break;
			case CharacterState.DoubleJump:
			case CharacterState.Collision:
			case CharacterState.Defeated:
			case CharacterState.Virtory:
				return;
		}

		Vector3 vPos = transform.position;
		Vector3 vTarget = vPos + Vector3.up * m_JumpPower;
		m_IsJumpStart = true;

        m_GameManager.PlaySound(EffectSound.Jump);
	}

    public void JumpEndDown()
	{
        SetAniState(CharacterState.Landing);
	}

	public void LandingEnd()
	{
		SetAniState(m_NextState);
	}

	public void OnPress_LinkButton(bool isPress)
	{
		switch (m_State)
		{
			case CharacterState.Collision:
			case CharacterState.Defeated:
			case CharacterState.Start:
			case CharacterState.Virtory:
				return;
		}

		if (!m_CurrentBehaviour.IsImpossibleProcessing(m_State) &&
		    !m_CurrentBehaviour.IsChoice(isPress))
			ChangeBehaviour();
	}

	public void ChangeBehaviour()
	{
		if (m_CurrentBehaviour.Equals(m_HumunBehaviour))
		{
			m_CurrentBehaviour.SetSelectEnd();
			m_CurrentBehaviour = m_LinkBehaviour;
		}
		else if (m_CurrentBehaviour.Equals(m_LinkBehaviour))
		{
			m_CurrentBehaviour.SetSelectEnd();
			m_CurrentBehaviour = m_HumunBehaviour;
		}
		
		m_CurrentBehaviour.SetSelectBehavior();
		m_CurrentBehaviour.SetAniMation(m_SpriteAni, m_State);
	}

	public void SetAniState(CharacterState State)
	{
		if (m_State.Equals(State))
			return;

		m_PrevState = m_State;
		m_State = State;
		m_CurrentBehaviour.SetAniMation(m_SpriteAni, m_State);
	}

    public void CollisionAniEnd()
    {
	    if (m_CurrentBehaviour.IsImpossibleProcessing(m_State))
		    ChangeBehaviour();
	    
        SetAniState(CharacterState.Defeated);
    }

    public void DefeatedAniEnd()
    {
		m_GameManager.StageFailed();
    }

	public void VirtoryAniEnd()
	{
		m_GameManager.ScrollStop();
		m_GameManager.StageClear();
	}

	public void SetCollisionObject(InGameObjectBase ObjBase)
	{
		if (ObjBase == null)
			return;

        m_CurrentBehaviour.CollisionObject(ObjBase, m_State);
	}

	public void StageFailed()
	{
		m_GameManager.StageFailed();
	}
}
