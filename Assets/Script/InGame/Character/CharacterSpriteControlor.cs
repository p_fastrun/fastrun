﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;

public class CharacterSpriteControlor
{
	private const string SELECT_SEX = "SELECT_SEX";
	private const string SELECT_DRESS = "SELECT_DRESS";
	protected const string ATLAS_PATH = "Atlas/Character/";
	protected const string LINKATLAS_PATH = "Atlas/Link/";

	protected MainGameManager m_GameManager;
	protected MainGameCharacter m_GameCharacter;
	protected UIAtlas m_Atlas;
	protected string m_AniStartName;

	private bool IsCollision = false;

	public static CharacterSpriteControlor CreateFactory(MainGameManager GameManager,
		MainGameCharacter gameCharacter, int Type)
	{
		CharacterSpriteControlor csc = null;
		
		switch (Type)
		{
			case 0: 
				csc = new MainGameLinkDog();
				break;
			case 1:
				csc = new MainGameLinkEgle();
				break;
			case 2: 
				csc = new MainGameLinkTurtle();
				break;
			case 3: 
				csc = new MainGameLinkPegasus();
				break;
			case 4: 
				csc = new MainGameLinkUnicorn();
				break;
			case 5: 
				csc = new MainGameLinkDragon();
				break;
			default:
				csc = new CharacterSpriteControlor();
				break;
		}
		
		csc.Init(GameManager, gameCharacter);
		return csc;
	}

	public virtual void SetSelectBehavior()
	{
		m_GameCharacter.transform.localPosition = new Vector3(-150f, -130f);
		BoxCollider box = m_GameCharacter.GetComponent<BoxCollider>();
		
		box.size = new Vector3(40f , 70f, 0);
	}
	
	public virtual void SetSelectEnd()
	{
		
	}

	public virtual bool IsChoice(bool IsLinkPress)
	{
		return !IsLinkPress;
	}

	public virtual bool IsImpossibleProcessing(CharacterState state)
	{
		return false;
	}

    public virtual bool IsImpossibleJump()
    {
        return IsCollision;
    }

	public virtual void SetLinkPercent(float Percent)
	{
		
	}

	public void Init(MainGameManager GameManager, MainGameCharacter gameCharacter)
	{
		m_GameManager = GameManager;
		m_GameCharacter = gameCharacter;
		SetAtlas();
	}

	public List<CharacterColorInfo> GetColorInfos()
	{
		List<CharacterColorInfo> ColorInfos = new List<CharacterColorInfo>();


		for (int Index = 0; Index < m_Atlas.spriteList.Count; ++Index)
		{
			ColorInfos.Add(new CharacterColorInfo(m_Atlas, m_Atlas.spriteList[Index].name));
		}

		return ColorInfos;
	}

	public void SetAniMation(UISpriteAnimation SpriteAni, CharacterState state)
	{

		UISprite sprite = SpriteAni.GetComponent<UISprite>();
		if (sprite != null && !sprite.atlas.Equals(m_Atlas))
		{
			sprite.atlas = m_Atlas;
            m_GameManager.AddCharacterEffect(InGameObjectType.Transformation00);

        }

		SetAni(SpriteAni, state);
	}

	protected virtual void SetAni(UISpriteAnimation SpriteAni, CharacterState state)
	{
		int Frames = 0;
		string AniName = string.Empty;
		
		switch (state)
		{
			case CharacterState.Run:
				AniName = "달리기";
				Frames = 5;
				break;
			case CharacterState.Jump:
				AniName = "달리기2";
				Frames = 1;
				break;
			case CharacterState.DoubleJump:
				AniName = "2단점프";
				Frames = 1;
				break;
			case CharacterState.Landing:
				AniName = "착지";
				Frames = 1;
				break;
			case CharacterState.Collision:
				AniName = "충돌";
				Frames = 5;
				SpriteAni.loop = false;
				SpriteAni.m_EndCallBack = m_GameCharacter.CollisionAniEnd;
				break;
			case CharacterState.Defeated:
				AniName = "패배";
				Frames = 5;
				SpriteAni.loop = false;
				SpriteAni.m_EndCallBack = m_GameCharacter.DefeatedAniEnd;
				break;
			case CharacterState.Virtory:
				AniName = "승리";
				Frames = 5;
				SpriteAni.loop = false;
				SpriteAni.m_EndCallBack = m_GameCharacter.VirtoryAniEnd;
				break;
		}

		SpriteAni.framesPerSecond = Frames;
		SpriteAni.namePrefix = m_AniStartName + AniName;
		SpriteAni.RebuildSpriteList();
		SpriteAni.ResetToBeginning();
	}

	public void CollisionObject(InGameObjectBase ObjBase, CharacterState Stage)
	{
		if (ObjBase == null)
			return;

		switch (ObjBase.m_ObjectType)
		{
			case InGameObjectType.Ground:
				
				break;
			case InGameObjectType.AniTrap:
			case InGameObjectType.Trap:
			{
				if (Stage.Equals(CharacterState.Collision) ||
				    Stage.Equals(CharacterState.Defeated))
				{
					return;
				}

				CollisionTrap(m_GameManager, ObjBase);
			}
				break;
			case InGameObjectType.Coin:
			{
				m_GameManager.AddCoin();
				ObjBase.ReturnObjectPool();
			}
				break;

			case InGameObjectType.Goal:
			{
				if (Stage.Equals(CharacterState.Virtory))
				{
					return;
				}
				
				CollisionGoal(ObjBase);
			}
				break;
		}
	}
	
	protected virtual void SetAtlas()
	{
		int Sex = Util.GetIntData(SELECT_SEX);
		int Dress = Util.GetIntData(SELECT_DRESS);

		string AtlasName = string.Empty;
		switch (Sex)
		{
			case 0: 
				AtlasName = "Male_";
				m_AniStartName = "남캐-";
				break;
			case 1: 
				AtlasName = "Female_"; 
				m_AniStartName = "여캐-";
				break;
		}

		switch (Dress)
		{
			case 0: AtlasName += "Base"; break;
			case 1: 
				AtlasName += "Normal";
				m_AniStartName += "기본-";
				break;
			case 2: 
				AtlasName += "Custum";
				m_AniStartName += "커스텀-";
				break;
		}
		
		m_Atlas = Resources.Load<UIAtlas>(ATLAS_PATH + AtlasName);
		m_GameCharacter.m_Sprite.atlas = m_Atlas;
	}

    protected void CollisionCoin(InGameObjectBase ObjBase)
    {
        m_GameManager.AddCoin();

        ObjBase.ReturnObjectPool();
    }
	
	protected virtual void CollisionTrap(MainGameManager GameManager, InGameObjectBase ObjBase)
	{
		IsCollision = true;
		GameManager.ScrollStop();
		m_GameCharacter.SetAniState(CharacterState.Collision);
        m_GameCharacter.m_NextState = CharacterState.Collision;
    }

	protected void CollisionGoal(InGameObjectBase ObjBase)
	{
		m_GameManager.ScrollStop();
		m_GameCharacter.SetAniState(CharacterState.Virtory);

        UISpriteAnimation ani = ObjBase.gameObject.AddComponent<UISpriteAnimation>();

        ani.loop = false;
        ani.framesPerSecond = 5;
        ani.namePrefix = "골인깃발";

    }
}
