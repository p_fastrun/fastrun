﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameManager : MonoBehaviour
{
	public MainGameMapProcess m_MapProcess;
	public MainGameObjectPool m_ObjectPool;
	public MainGameUILinkButton m_LinkButton;
	public MainGameCharacter m_Character;
	public UITexture[] m_BGScroll;
	public GameObject m_GameOver;
	public GameObject m_GameClear;
	public int m_UseLinkCoin;

    public UILabel m_Version;
    public UILabel m_FPS;

	private const string EPISODE_STAGEKEY = "EPISODE_STAGEKEY";
    private const string PLAY_EPISODE_STAGEKEY = "PLAY_EPISODE_STAGE";

    private GameMapParser m_MapParser = new GameMapParser();
    private SoundManager m_SoundManager;

    private List<CharacterColorInfo> m_ColorInfolist = new List<CharacterColorInfo>(); 
	private int m_StageID;
	private int m_CoinCount;
	private float m_CoinDumy;
	private bool StageEndEffect = false;
	private bool m_isUseLinkCoin = false;

	private const float ScrollBGTime = 4f;
    private const string FPSText = "FPS: ";


    public void Awake()
	 {
		m_StageID = Util.GetIntData(PLAY_EPISODE_STAGEKEY);

        m_SoundManager = GameObject.FindObjectOfType<SoundManager>();

        if (m_SoundManager == null)
        {
            GameObject soundManager = new GameObject("SoundManager");
            m_SoundManager = soundManager.AddComponent<SoundManager>();
        }

        m_SoundManager.SoundPlay(BGMFlag.Main_BGM);

        if (2 >= m_StageID % 10)
        {
            m_SoundManager.SoundPlay(BGMFlag.EP01_03_BGM);
        }
        else
        {
            m_SoundManager.SoundPlay(BGMFlag.EP01_01_BGM);
        }

        //맵 로드
        m_MapParser.LoadMap(m_StageID);
		//캐릭터 초기화
		m_Character.Init(this);
		//링크 버튼 초기화
		m_LinkButton.Init(this);
		
		m_ColorInfolist.Clear();
		m_ColorInfolist.AddRange(m_Character.GetColorInfos());
		m_ColorInfolist.AddRange(m_MapParser.GetColorInfos(m_ObjectPool.m_AtlasList));

		SetBGTexture();
	}

	public void Update()
	{
		Vector2 vMinMax = m_MapProcess.MoveMapResult();
		List<InGameObjectData> dataList = m_MapParser.GetRangeObject(vMinMax.x, vMinMax.y);
		List<InGameObjectBase> ObjbaseList = m_ObjectPool.GetObject(dataList);
		
		m_MapProcess.SetMapObject(ObjbaseList);

		CheckCollision();

		if (m_isUseLinkCoin)
		{
			m_CoinDumy -= m_UseLinkCoin * Time.deltaTime;
			m_CoinCount = (int)m_CoinDumy;


            m_Character.SetLinkPercent(m_CoinDumy / (float)MainGameUILinkButton.LINK_MAX);
			m_LinkButton.AddCoin(m_CoinDumy);
		}
#if UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Space))
		{
			OnClick_Jump();
		}
		
		if (Input.GetKeyDown(KeyCode.Z))
		{
			OnPress_LinkButton(true);
		}
		
		if (Input.GetKeyDown(KeyCode.F5))
		{
			Util.LoadLevel("MainGameScene");
		}

        if (Input.GetKeyDown(KeyCode.F6))
        {
            StageClear();
        }
#endif

        if (m_FPS != null && m_FPS.gameObject.activeInHierarchy)
        {
            int fps = (int)(1f / Time.deltaTime);
            m_FPS.text = FPSText + fps;
        }
        
    }

    

	private void SetBGTexture()
	{
		Texture2D tex = Resources.Load<Texture2D>("Texture/" + m_StageID.ToString().Replace("0", "-"));
		for (int Index = 0; Index < m_BGScroll.Length; ++Index)
		{
			m_BGScroll[Index].mainTexture = tex;
		}
	}

	private void CheckCollision()
	{
		BoxCollider CharBox = m_Character.GetComponent<BoxCollider>();
		List<InGameObjectBase> ObjectList = m_MapProcess.GetMapObjectList();
		CharacterColorInfo ColorInfo = m_ColorInfolist.Find(d => d.m_SpriteName.Equals(m_Character.GetSPriteName()));
		Vector3 vPos = m_Character.transform.localPosition;
		
		
		for (int Index = 0; Index < ObjectList.Count; ++Index)
		{
            if (ObjectList[Index].m_ObjectType.Equals(InGameObjectType.Ground))
                continue;


            Bounds ObjBox = ObjectList[Index].m_BoxCollider.bounds;

			if (!CharBox.bounds.Intersects(ObjBox))
				continue;
			
			if(ObjectList[Index].m_ObjectType.Equals(InGameObjectType.AniTrap) ||
				ObjectList[Index].m_ObjectType.Equals(InGameObjectType.Trap))
			{
				CharacterColorInfo ObjColorInfo = m_ColorInfolist.Find(d => 
					d.m_SpriteName.Equals(ObjectList[Index].GetSpriteName()));

				if (ObjColorInfo == null)
				{
					//Debug.LogError(string.Format("Sprite: {0} IS Notting" , ObjectList[Index].GetSpriteName()));
					continue;
				}

				Vector3 vObjPos = ObjectList[Index].transform.localPosition;
			
				//if(ColorInfo != null && !ColorInfo.CheckPixcelCollision(ObjColorInfo, vPos, vObjPos))
				//	continue;
			}
			
			m_Character.SetCollisionObject(ObjectList[Index]);
		}
	}

	public void OnClick_Jump()
	{
		m_Character.OnClick_Jump();
		
	}

	public void AddCoin()
	{
		m_CoinCount++;
		m_LinkButton.AddCoin(m_CoinCount);
        PlaySound(EffectSound.Coin);
	}

	public int UseCoin()
	{
		m_CoinCount -= m_UseLinkCoin;
		m_CoinCount = m_CoinCount < 0 ? 0 : m_CoinCount;
		return m_CoinCount;
	}

	public void StageFailed()
	{
		m_GameOver.SetActive(true);

		TweenPosition TP = m_GameOver.GetComponentInChildren<TweenPosition>();

		TP.method = UITweener.Method.BounceIn;
		TP.callWhenFinished = "OnStageEndEffect";
		TP.eventReceiver = gameObject;
	}

	public void StageClear()
	{
		int StageCheck = m_StageID % 100;

		if (StageCheck >= 4)
		{
			m_StageID = m_StageID / 100 * 100 + 101;
		}
		else
		{
			++m_StageID;
		}
		
		Util.SaveData(EPISODE_STAGEKEY, m_StageID);

		m_GameClear.SetActive(true);
		TweenScale TS = m_GameClear.GetComponentInChildren<TweenScale>();
		TS.method = UITweener.Method.BounceIn;
		TS.callWhenFinished = "OnStageEndEffect";
		TS.eventReceiver = gameObject;
	}

	public void OnStageEndEffect()
	{
		StageEndEffect = true;
	}

	public void GotoLobbyScene()
	{
		if (StageEndEffect == false)
			return;
		
		Util.LoadLevel("LobbyScene");
	}

	public void ScrollSpeedUp()
	{
		m_MapProcess.SpeedUp();
		
		TweenPosition TP = m_BGScroll[0].GetComponent<TweenPosition>();
		TP.duration = ScrollBGTime * 0.5f;
	}
	
	public void ScrollSpeedDown()
	{
		m_MapProcess.SpeedDown();
		
		TweenPosition TP = m_BGScroll[0].GetComponent<TweenPosition>();
		TP.duration = ScrollBGTime * 2f;
	}

	public void ScrollSpeedNormal()
	{
		m_MapProcess.SpeedNormal();
		
		TweenPosition TP = m_BGScroll[0].GetComponent<TweenPosition>();
		TP.duration = ScrollBGTime;
	}

	public void ScrollStop()
	{
		m_MapProcess.Stop();

		TweenPosition TP = m_BGScroll[0].GetComponent<TweenPosition>();

		TP.enabled = false;
	}

	public void OnPress_LinkButton(bool isPress)
	{
		if (m_isUseLinkCoin != isPress)
		{
			m_isUseLinkCoin = isPress;

			if (m_isUseLinkCoin == true)
			{
				m_CoinDumy = MainGameUILinkButton.LINK_MAX;
			}
		}
		
		m_LinkButton.AddCoin(m_CoinCount);
		m_Character.OnPress_LinkButton(isPress);
	}

    public void PlaySound(EffectSound soundType)
    {
        m_SoundManager.SoundPlay(soundType);
    }

    public void AddCharacterEffect(InGameObjectType ObjType)
    {
        m_ObjectPool.ParentInObject(m_MapProcess.transform, m_Character.transform.localPosition, ObjType);
    }
}
