﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameMapProcess : MonoBehaviour
{
	public float MoveSpeed;


	private float m_MoveSpeed;
	private const float ScopeHalf = Util.ScreenX * 0.75f;
	private Transform MapTrans;
	private List<InGameObjectBase> m_MapObjectList = new List<InGameObjectBase>();
	
	private void Awake()
	{
		MapTrans = transform;
		m_MoveSpeed = MoveSpeed;
	}

	public List<InGameObjectBase> GetMapObjectList()
	{
		return m_MapObjectList;
	}

	public Vector2 MoveMapResult()
	{
		MapTrans.localPosition -= Vector3.right * m_MoveSpeed;
		RemoveCheck();
		return GetMinMax();
	}

	public void SpeedUp()
	{
		if (m_MoveSpeed == 0)
			return;
		
		m_MoveSpeed = MoveSpeed * 2f;
	}

	public void SpeedNormal()
	{
		if (m_MoveSpeed == 0)
			return;
		
		m_MoveSpeed = MoveSpeed;
	}
	
	public void SpeedDown()
	{
		if (m_MoveSpeed == 0)
			return;
		
		m_MoveSpeed = MoveSpeed * 0.5f;
	}

	public void Stop()
	{
		m_MoveSpeed = 0;
	}

	public void SetMapObject(List<InGameObjectBase> objectBases)
	{
		for (int Index = 0; Index < objectBases.Count; ++Index)
		{
			objectBases[Index].SetParent(MapTrans);
		}
		
		m_MapObjectList.AddRange(objectBases);
	}

	private void RemoveCheck()
	{
		float PosX = MapTrans.localPosition.x;
		
		List<InGameObjectBase> removeList = new List<InGameObjectBase>();
			
		for (int Index = 0; Index < m_MapObjectList.Count; ++Index)
		{
			float CheckValue = m_MapObjectList[Index].transform.localPosition.x + PosX;

			if (-ScopeHalf <= CheckValue)
				continue;

			removeList.Add(m_MapObjectList[Index]);
		}

		for (int Index = 0; Index < removeList.Count; ++Index)
		{
			m_MapObjectList.Remove(removeList[Index]);
			removeList[Index].ReturnObjectPool();
		}
	}

    public void RemoveObject(InGameObjectBase Obj)
    {
        if (m_MapObjectList.Contains(Obj))
            m_MapObjectList.Remove(Obj);
    }

	private Vector2 GetMinMax()
	{
		Vector2 vMinMax = Vector2.zero;

		vMinMax.x = MapTrans.localPosition.x - ScopeHalf;
		vMinMax.y = MapTrans.localPosition.x + ScopeHalf;

		return vMinMax;
	}
}
