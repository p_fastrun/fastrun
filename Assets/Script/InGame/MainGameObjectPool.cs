﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameObjectPool : MonoBehaviour
{
	public UIAtlas[] m_AtlasList;
	public GameObject m_BasePrefab;
	public GameObject m_CoinPrefab;
	public GameObject m_GoalPrefab;
	public GameObject m_AniTrapPrefab;

    public GameObject m_TrapDestory01;
    public GameObject m_TrapDestory02;
    public GameObject m_Transformation00;
    public GameObject m_ShieldEffect00;
    public GameObject m_LightEffect;
    public GameObject m_DogEffect;
    public GameObject m_JellyEffect;
	
	
	private Dictionary<InGameObjectType, List<InGameObjectBase>> m_ObjectPoolList = 
		new Dictionary<InGameObjectType, List<InGameObjectBase>>();

    private void Awake()
    {
        for (int Index = 0; Index < 5; ++Index)
        {            
            AddObject(InGameObjectType.TrapDestory01);
            AddObject(InGameObjectType.TrapDestory02);
            AddObject(InGameObjectType.Transformation00);
            AddObject(InGameObjectType.ShieldEffect00);
            AddObject(InGameObjectType.LightEffect);
            AddObject(InGameObjectType.DogEffect);
            AddObject(InGameObjectType.JellyEffect);            
        }
    }

    private void AddObject(InGameObjectType ObjType)
    {
        InGameObjectBase ObjBase = GetObject(ObjType);
        ObjBase.SetParent(transform);
        AddObject(ObjBase);
    }

    public void AddObject(InGameObjectBase InGameObj)
	{
		if (!m_ObjectPoolList.ContainsKey(InGameObj.m_ObjectType))
		{
            m_ObjectPoolList.Add(InGameObj.m_ObjectType, new List<InGameObjectBase>());
		}
		
		m_ObjectPoolList[InGameObj.m_ObjectType].Add(InGameObj);
	}

	public List<InGameObjectBase> GetObject(List<InGameObjectData> datas)
	{
		List<InGameObjectBase> ObjBaseList = new List<InGameObjectBase>();
		for (int Index = 0; Index < datas.Count; ++Index)
		{
			InGameObjectType Objtype = (InGameObjectType) datas[Index].ObjectType;

			InGameObjectBase ObjBase = GetObject(Objtype);
			ObjBase.SetData(m_AtlasList[datas[Index].AtlasIdx], datas[Index]);
			ObjBaseList.Add(ObjBase);
		}
		return ObjBaseList;
	}

    public void ParentInObject(Transform trans, Vector3 vPos, InGameObjectType Objtype)
    {
        InGameObjectBase ObjBase = GetObject(Objtype);

        ObjBase.transform.localPosition = vPos;
        ObjBase.SetParent(trans);
        ObjBase.Reset();
    }

	private InGameObjectBase GetObject(InGameObjectType Objtype)
	{
		List<InGameObjectBase> values;
		if (m_ObjectPoolList.TryGetValue(Objtype, out values))
		{
			if (values.Count > 0)
			{
				InGameObjectBase Obj = values[0];

                if (Obj.transform.localPosition.x != 0)
                {
                    int a = 0;
                }

                m_ObjectPoolList[Objtype].Remove(Obj);
				return Obj;
			}
		}

		GameObject Clone = null;

		switch (Objtype)
		{
			
			case InGameObjectType.Ground:
			case InGameObjectType.Trap:
				Clone = GameObject.Instantiate(m_BasePrefab);
				break;
			case InGameObjectType.Coin:
				Clone = GameObject.Instantiate(m_CoinPrefab);
				break;
			case InGameObjectType.Goal:
				Clone = GameObject.Instantiate(m_GoalPrefab);
				break;
			case InGameObjectType.AniTrap:
				Clone = GameObject.Instantiate(m_AniTrapPrefab);
				break;
            case InGameObjectType.TrapDestory01:
                Clone = GameObject.Instantiate(m_TrapDestory01);
                break;
            case InGameObjectType.TrapDestory02:
                Clone = GameObject.Instantiate(m_TrapDestory02);
                break;
            case InGameObjectType.Transformation00:
                Clone = GameObject.Instantiate(m_Transformation00);
                break;
            case InGameObjectType.ShieldEffect00:
                Clone = GameObject.Instantiate(m_ShieldEffect00);
                break;
            case InGameObjectType.LightEffect:
                Clone = GameObject.Instantiate(m_LightEffect);
                break;
            case InGameObjectType.DogEffect:
                Clone = GameObject.Instantiate(m_DogEffect);
                break;
            case InGameObjectType.JellyEffect:
                Clone = GameObject.Instantiate(m_JellyEffect);
                break;
        }
        
        InGameObjectBase ObjBase = Clone.GetComponent<InGameObjectBase>();
		ObjBase.m_ObjectType = Objtype;
		ObjBase.Init(this);
		return ObjBase;
	}
}
