﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InGameObjectType
{
	Ground,
	Trap,
	Coin,
    AniTrap,
    Goal,
    TrapDestory01,
    TrapDestory02,
    Transformation00,
    ShieldEffect00,
    LightEffect,
    DogEffect,
    JellyEffect,
}

[System.Serializable]
public class InGameObjectData
{
	public float PosX;
	public float PosY;
	public int ObjectType;
	public string spriteName;
	public int AtlasIdx;

	public string Tostring()
	{
		return string.Format("PosX: {0}, PosY: {1}, ObjectTYpe: {2}, SpriteName: {3}, AtlasIdx: {4}",
			PosX, PosY, ObjectType, spriteName, AtlasIdx);
	}
}

public class InGameObjectBase : MonoBehaviour
{
	public InGameObjectType m_ObjectType;
	public UISprite m_Sprite;
	public BoxCollider m_BoxCollider;

	protected MainGameObjectPool m_ObjectPool;

	public string GetSpriteName()
	{
		return m_Sprite.spriteName;
	}

	public void Init(MainGameObjectPool ObjectPool)
	{
		m_ObjectPool = ObjectPool;
	}

	public void SetParent(Transform parent)
	{
		Vector3 vPos = transform.localPosition;
		transform.parent = parent;
		transform.localPosition = vPos;
		transform.localScale = Vector3.one;
		
		m_Sprite.ParentHasChanged();
	}

    public virtual void Reset() { }


    public virtual void SetData(UIAtlas atlas , InGameObjectData data)
	{
		transform.localPosition = new Vector3(data.PosX, data.PosY);
		m_ObjectType = (InGameObjectType)data.ObjectType;
		m_Sprite.atlas = atlas;
		m_Sprite.spriteName = data.spriteName;
		m_Sprite.MakePixelPerfect();
		m_Sprite.ResizeCollider();

        if (m_ObjectType.Equals(InGameObjectType.Goal))
        {
            m_Sprite.spriteName = "골인깃발1";
        }

		Vector3 vBox = m_BoxCollider.size;
		vBox.z = 10;
		m_BoxCollider.size = vBox;
		
	}

	public void ReturnObjectPool()
	{
		if (m_ObjectPool == null)
			return;

        switch (m_ObjectType)
        {
            case InGameObjectType.Coin:
                {
                    m_ObjectPool.ParentInObject(transform.parent, transform.localPosition, InGameObjectType.JellyEffect);
                }
                break;
            case InGameObjectType.Trap:
            case InGameObjectType.AniTrap:
                {
                    if (m_Sprite.spriteName.Contains("1단"))
                        m_ObjectPool.ParentInObject(transform.parent, transform.localPosition, InGameObjectType.TrapDestory01);
                    else
                        m_ObjectPool.ParentInObject(transform.parent, transform.localPosition, InGameObjectType.TrapDestory02);
                }
                break;
        }

        MainGameMapProcess mapProcess = transform.parent.GetComponent<MainGameMapProcess>();
        mapProcess.RemoveObject(this);



        transform.localPosition = Vector3.zero;
		SetParent(m_ObjectPool.transform);
		m_ObjectPool.AddObject(this);
	}

	public InGameObjectData GetObjectData()
	{
		InGameObjectData data = new InGameObjectData();

		data.PosX = transform.localPosition.x;
		data.PosY = transform.localPosition.y;
		data.ObjectType = (int)m_ObjectType;
		data.spriteName = m_Sprite != null ? m_Sprite.spriteName : string.Empty;

		if (m_Sprite != null && m_Sprite.atlas != null)
		{
			switch (m_Sprite.atlas.name)
			{
				case "Goal": data.AtlasIdx = 0; break;
				case "Episode1-1": data.AtlasIdx = 1; break;
				case "Episode1-2": data.AtlasIdx = 2; break;
				case "Episode1-3": data.AtlasIdx = 3; break;
				case "Episode1-4": data.AtlasIdx = 4; break;
				case "Episode2-1": data.AtlasIdx = 5; break;
				case "Episode2-2": data.AtlasIdx = 6; break;
				case "Episode2-3": data.AtlasIdx = 7; break;
				case "Episode2-4": data.AtlasIdx = 8; break;
				case "Episode3-1": data.AtlasIdx = 9; break;
				case "Episode3-2": data.AtlasIdx = 10; break;
				case "Episode3-3": data.AtlasIdx = 11; break;
				case "Episode3-4": data.AtlasIdx = 12; break;
				case "LobbyUI": data.AtlasIdx = 13; break;
			}
		}
		
		
		return data;
	}
	
}
