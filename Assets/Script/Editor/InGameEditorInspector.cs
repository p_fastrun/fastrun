﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(InGameEditor), true)]
public class InGameEditorInspector : Editor
{

	private const string GROUND_BUTTONTEXT = "({0}) 이미지로 바닥 생성";
	private const string CREATE_TRAP_GROUND = "({0}) 이미지로 땅위에 생성";
    private const string CREATE_TRAP_MIDDLE = "({0}) 이미지로 공중에 생성";
    private const string CREATE_TRAP_High = "({0}) 이미지로 천장에 생성";
	private const string CREATE_COIN = "코인 생성. Y: {0}";
	

	public override void OnInspectorGUI ()
	{
		InGameEditor Edit = serializedObject.targetObject as InGameEditor;
		
		if (NGUIEditorTools.DrawHeader("데이타 셋팅"))
		{
			SerializedProperty ObjectPrefab = NGUIEditorTools.DrawProperty("오브젝트 프리팹", serializedObject, 
				"m_ObjectPrefab", GUILayout.MinWidth(20f));

			if (ObjectPrefab != null)
			{
				SerializedProperty sp = serializedObject.FindProperty("m_ObjectPrefab");
				sp.objectReferenceValue = ObjectPrefab.objectReferenceValue;
				serializedObject.ApplyModifiedProperties();
			}
			
			SerializedProperty CoinPrefab = NGUIEditorTools.DrawProperty("코인 프리팹", serializedObject, 
				"m_CoinPrefab", GUILayout.MinWidth(20f));

			if (CoinPrefab != null)
			{
				serializedObject.ApplyModifiedProperties();
			}
			
			SerializedProperty GoalPrefab = NGUIEditorTools.DrawProperty("깃발 프리팹", serializedObject, 
				"m_GoalPrefab", GUILayout.MinWidth(20f));

			if (GoalPrefab != null)
			{
				serializedObject.ApplyModifiedProperties();
			}
			
			SerializedProperty Atlas = NGUIEditorTools.DrawProperty("아틀라스", serializedObject, 
				"m_Atlas", GUILayout.MinWidth(20f));
			
			if (Atlas != null)
			{
				SerializedProperty sp = serializedObject.FindProperty("m_Atlas");
				sp.objectReferenceValue = Atlas.objectReferenceValue;
				serializedObject.ApplyModifiedProperties();
			}
		
			SerializedProperty ObjectRoots = NGUIEditorTools.DrawProperty("오브젝트 루트", serializedObject, 
				"m_ObjectRoots", GUILayout.MinWidth(20f));
			
			if (ObjectRoots != null)
			{
				serializedObject.ApplyModifiedProperties();
			}
			
			SerializedProperty CoinPosY = NGUIEditorTools.DrawProperty("코인 기본Y좌표", serializedObject, 
				"m_CoinYPositions", GUILayout.MinWidth(20f));
			
			if (CoinPosY != null)
			{
				serializedObject.ApplyModifiedProperties();
			}
		
		}

		if (NGUIEditorTools.DrawHeader("바닥생성관련"))
		{
			SerializedProperty GroudSize = NGUIEditorTools.DrawProperty("바닥 길이", serializedObject, 
				"m_GroudSize", GUILayout.MinWidth(20f));

			SerializedProperty sp = serializedObject.FindProperty("m_SpriteName");
			string ButtonText = string.Format(GROUND_BUTTONTEXT, sp.stringValue);
			
			if (GUILayout.Button(ButtonText, GUILayout.MinWidth(40f)))
			{
				Edit.SetGroundObject();
			}

            if (GUILayout.Button("선택한 트랙에 깃발 생성", GUILayout.MinWidth(40f)))
            {
                Vector3 vPos = Vector3.zero;

                GameObject SelectObj = SelectObjectCheck("땅을 한개만 선택하세요");

                if (SelectObj != null)
                {
                    InGameObjectBase ObjBase = SelectObj.GetComponent<InGameObjectBase>();

                    if (ObjBase.m_ObjectType == InGameObjectType.Ground)
                    {
                        vPos.x = ObjBase.transform.localPosition.x;
                        float GroundTop = ObjBase.transform.localPosition.y + (ObjBase.m_Sprite.height * 0.5f);
                        GroundTop += 144f;

                        vPos.y = GroundTop;
                    }
	                Edit.CreateGoalObject(vPos);
                }
            }
        }

		if (NGUIEditorTools.DrawHeader("트랩 생성 관련"))
		{
			if (GUILayout.Button(string.Format(CREATE_TRAP_GROUND, Edit.m_SpriteName), GUILayout.MinWidth(40f)))
			{
				Vector3 vPos = Vector3.zero;
				
				GameObject SelectObj = SelectObjectCheck("땅을 한개만 선택하세요");

				if (SelectObj != null)
				{
					InGameObjectBase ObjBase = SelectObj.GetComponent<InGameObjectBase>();

					if (ObjBase.m_ObjectType == InGameObjectType.Ground)
					{
						vPos.x = ObjBase.transform.localPosition.x;
						float GroundTop = ObjBase.transform.localPosition.y + (ObjBase.m_Sprite.height * 0.5f);
						UISpriteData data = Edit.m_Atlas.GetSprite(Edit.m_SpriteName);
						
						if (data != null)
						{
							GroundTop += data.height * 0.5f;
							GroundTop -= 20f;
						}
						
						vPos.y = GroundTop;
					}
					Edit.CreateTrabObject(vPos);
				}
			}

            if (GUILayout.Button(string.Format(CREATE_TRAP_MIDDLE, Edit.m_SpriteName), GUILayout.MinWidth(40f)))
            {
                Vector3 vPos = Vector3.zero;

	            GameObject SelectObj = SelectObjectCheck("땅을 한개만 선택하세요");
                if (SelectObj != null)
                {
                    InGameObjectBase ObjBase = SelectObj.GetComponent<InGameObjectBase>();

                    if (ObjBase.m_ObjectType == InGameObjectType.Ground)
                    {
                        vPos.x = ObjBase.transform.localPosition.x;                        
                        vPos.y = -90f;
                    }
	                Edit.CreateTrabObject(vPos);
                }
            }

            if (GUILayout.Button(string.Format(CREATE_TRAP_High, Edit.m_SpriteName), GUILayout.MinWidth(40f)))
			{
				Vector3 vPos = Vector3.zero;

				GameObject SelectObj = SelectObjectCheck("땅을 한개만 선택하세요");
				if (SelectObj != null)
				{
					InGameObjectBase ObjBase = SelectObj.GetComponent<InGameObjectBase>();

					if (ObjBase.m_ObjectType == InGameObjectType.Ground)
					{
						vPos.x = ObjBase.transform.localPosition.x;
						UISpriteData data = Edit.m_Atlas.GetSprite(Edit.m_SpriteName);
						
						if (data != null)
						{
							vPos.y = 240f - (data.height * 0.5f);
						}
						Edit.CreateTrabObject(vPos);
					}
				}
			}
		}

		if (NGUIEditorTools.DrawHeader("코인 생성 관련"))
		{
			if (Edit.m_CoinYPositions != null)
			{
				for (int Index = 0; Index < Edit.m_CoinYPositions.Length; ++Index)
				{
					string ButtonText = string.Format(CREATE_COIN, Edit.m_CoinYPositions[Index]);
					if (GUILayout.Button(ButtonText, GUILayout.MinWidth(40f)))
					{
						GameObject SelectObj = SelectObjectCheck("땅을 한개만 선택하세요");
				
						Vector3 vPos = Vector3.zero;

						if (SelectObj != null)
						{
							InGameObjectBase ObjBase = SelectObj.GetComponent<InGameObjectBase>();

							if (ObjBase.m_ObjectType == InGameObjectType.Ground)
							{
								vPos.x = ObjBase.transform.localPosition.x;
								vPos.y = Edit.m_CoinYPositions[Index];
						
								Edit.CreateCoinObject(vPos);
							}
						}
					}
				}
			}
			
			
		}

		string[] SpriteList = Edit.m_Atlas.spriteList.ConvertAll<string>(d => d.name).ToArray();

		if (string.IsNullOrEmpty(Edit.m_SpriteName))
		{
			Edit.m_SpriteName = SpriteList[0];
		}

		Edit.m_SpriteName = NGUIEditorTools.DrawList("이미지 선택", SpriteList, Edit.m_SpriteName);
	}

	private GameObject SelectObjectCheck(string content)
	{
		if (Selection.gameObjects.Length == 1)
		{
			return Selection.gameObjects[0];
		}
		else if (Selection.gameObjects.Length > 1)
		{
			PopupMessage.ShowPopup(content);
			return null;
		}
		else
		{
			PopupMessage.ShowPopup(content);
			return null;
		}
	}

	public override bool HasPreviewGUI ()
	{
		InGameEditor Edit = target as InGameEditor;
		return Edit != null && !string.IsNullOrEmpty(Edit.m_SpriteName) && Edit.m_Atlas != null;
	}
	
	public override void OnPreviewGUI (Rect rect, GUIStyle background)
	{
		InGameEditor Edit = target as InGameEditor;

		if (Edit == null || Edit.m_Atlas == null)
			return;
		
		Texture2D tex = Edit.m_Atlas.spriteMaterial.mainTexture as Texture2D;
		if (tex == null) return;

		UISpriteData sd = Edit.m_Atlas.GetSprite(Edit.m_SpriteName);
		NGUIEditorTools.DrawSprite(tex, rect, sd, Color.white);
	}
}
