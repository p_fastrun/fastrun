﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class PopupMessage : EditorWindow
{
	public string m_Content;
	
	public static void ShowPopup(string content)
	{
		PopupMessage window = ScriptableObject.CreateInstance<PopupMessage>();
		window.position = new Rect(Screen.width / 2, Screen.height / 2, 250, 150);
		window.m_Content = content;
		window.Show();
	}

	void OnGUI()
	{
		EditorGUILayout.LabelField(m_Content, EditorStyles.wordWrappedLabel);
		GUILayout.Space(70);
		if (GUILayout.Button("확인")) this.Close();
	}
}
