﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public static class PastRunEditorMenu 
{

	[MenuItem("FastRun/에피소드 Data 출력", false, 0)]
	public static void EpisodeDataExport ()
	{
		InGameEditor inGameEditor = GameObject.FindObjectOfType<InGameEditor>();

		if (inGameEditor == null)
		{
			Debug.LogError("InGameEditor를 찾을수 없습니다.");
			return;
		}
		
		List<InGameObjectData> DataList = new List<InGameObjectData>();

		for (int Index = 0; Index < inGameEditor.m_ObjectRoots.Length; ++Index)
		{
			for (int Index2 = 0; Index2 < inGameEditor.m_ObjectRoots[Index].childCount; ++Index2)
			{
				Transform child = inGameEditor.m_ObjectRoots[Index].GetChild(Index2);

				if (child == null)
					continue;

				InGameObjectBase objectBase = child.GetComponent<InGameObjectBase>();

				if (objectBase == null)
					continue;


				DataList.Add(objectBase.GetObjectData());
			}
		}

		string SavePath = System.Environment.CurrentDirectory + "/Assets/Resources/GameData/";

		SavePath += SceneManager.GetActiveScene().name + ".txt";
		System.IO.StreamWriter writer = new System.IO.StreamWriter(SavePath);
		System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bin = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
		bin.Binder = new BinalryBinder();
		bin.Serialize(writer.BaseStream, DataList.ToArray());
		writer.Close();
		AssetDatabase.Refresh();
	}

	[MenuItem("FastRun/로컬 저장 임시 데이터 삭제", false, 1)]
	public static void EpisodeDataExport2()
	{
		PlayerPrefs.DeleteAll();
	}

    [MenuItem("FastRun/모든 스테이지 클리어", false, 2)]
    public static void EpisodeDataSave()
    {
        PlayerPrefs.SetInt("EPISODE_STAGEKEY", 401);
    }
}

public class BinalryBinder : System.Runtime.Serialization.SerializationBinder
{

	public override System.Type BindToType(string assemblyName, string typeName)
	{
		string assem = System.Reflection.Assembly.GetExecutingAssembly().FullName;

		System.Type type = System.Type.GetType(string.Format("{0}, {1}", typeName, assem));

		return type;
	}
}