﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BGMFlag
{
    Main_BGM,
    EP01_01_BGM,
    EP01_03_BGM,
    Max,
}

public enum EffectSound
{
    Click,
    Coin,
    Jump,
    Lose,
    StoneHit,
    Upgrade,
    Max,
}

public class SoundManager : MonoBehaviour
{
    private List<AudioSource> m_BGMs = new List<AudioSource>();
    private List<AudioSource> m_EffectSound = new List<AudioSource>();

    private BGMFlag m_BGMFlag = BGMFlag.Max;
    const string DefaultPath = "Sound/";

    private void Awake()
    {
        DontDestroyOnLoad(this);

        for (int Index = 0; Index < (int)BGMFlag.Max; ++Index)
        {
            BGMFlag flag = (BGMFlag)Index;
            m_BGMs.Add( CreateAudioSource(flag.ToString() , true) );
        }

        for (int Index = 0; Index < (int)EffectSound.Max; ++Index)
        {
            EffectSound flag = (EffectSound)Index;
            m_EffectSound.Add(CreateAudioSource(flag.ToString(), false));
        }
    }

    private AudioSource CreateAudioSource(string AudioName, bool loop)
    {
        GameObject source = new GameObject(AudioName);

        source.transform.parent = transform;

        AudioSource audiosource = source.AddComponent<AudioSource>();

        AudioClip clip = Resources.Load(DefaultPath + AudioName) as AudioClip;
        audiosource.clip = clip;
        audiosource.loop = loop;

        return audiosource;
    }

    public void SoundPlay(BGMFlag type)
    {
        int Index = (int)type;

        if (Index < 0 && Index >= m_BGMs.Count)
            return;

        for (int Index2 = 0; Index2 < m_BGMs.Count; ++Index2)
        {
            if (Index.Equals(Index2))
            {
                m_BGMs[Index2].Play();
            }
            else
                m_BGMs[Index2].Stop();
        }
    }

    public void SoundPlay(EffectSound type)
    {       
        int Index = (int)type;

        if (Index < 0 && Index >= m_EffectSound.Count)
            return;

        m_EffectSound[Index].Play();
    }
}
